//
//  main.m
//  Tags
//
//  Created by Manuel "StuFF mc" Carrasco Molina on 10/26/13.
//  Copyright (c) 2013 Manuel "StuFF mc" Carrasco Molina. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TGAppDelegate class]));
    }
}
